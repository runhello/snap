# snap

Time-travelling shooter game. This is just the source code-- the game itself is at runhello.com. Please see the wiki page.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/snap).**
